package com.example.sixthtask

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

data class Person(var firstName : String,
                  var lastName :String,
                  var email : String,
                  var birthYear : Int,
                  var gender : String) : Serializable
{
    var personFirstName = firstName
    var personLastName = lastName
    var personEmail =  email
    var personBirthYear = birthYear
    var personGender = gender
}
