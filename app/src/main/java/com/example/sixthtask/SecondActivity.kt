package com.example.sixthtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.CheckBox
import android.widget.Toast
import com.example.sixthtask.databinding.ActivitySecondBinding
import java.lang.Integer.parseInt

class SecondActivity : AppCompatActivity() {

    lateinit var binding: ActivitySecondBinding
    lateinit var gender : String
    lateinit var person : Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.save.setOnClickListener {
            save()
        }
    }

    private fun isLetters(string: String): Boolean {
        return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
    }

    private fun genderDetermine() : String {
        if (binding.female.isChecked){
            return "female"
        }
        else {
            return "male"
        }
    }

    private fun check() : Boolean{
        if(binding.first.text.toString().isBlank() || binding.last.text.toString().isBlank() || binding.birthy.text.toString().isBlank()
            || binding.em.text.toString().isBlank() || !Patterns.EMAIL_ADDRESS.matcher(binding.em.text.toString()).matches() ||
            !isLetters(binding.first.text.toString()) || !isLetters(binding.last.text.toString()) || parseInt(binding.birthy.text.toString()) < 1900
            || (!binding.male.isChecked && !binding.female.isChecked) || ((binding.male.isChecked && binding.female.isChecked))) {
            Toast.makeText(this, "To change profile info everything MUST be filled with valid info", Toast.LENGTH_SHORT).show()
            return true
        }
        person = Person(binding.first.text.toString(), binding.last.text.toString(), binding.em.text.toString(), parseInt(binding.birthy.text.toString()), genderDetermine())
        return false
    }


    private fun save(){
        if (check()) return
        else{
            val returnIntent = Intent()
            returnIntent.putExtra("NewPerson", person)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }
}