package com.example.sixthtask

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sixthtask.databinding.ActivitySplashScreenBinding

class SplashScreenActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

       /** ObjectAnimator.ofFloat(binding.welcomePic, "translationX", 100f).apply {
            duration = 2000
            start()
        }*/

        /**val set = AnimatorSet()
        val rotation = ObjectAnimator.ofFloat(binding.welcomePic, "rotation", -0.05f, 0.05f).apply {
            duration = 50
            repeatCount = 20
            repeatMode = ValueAnimator.REVERSE
        }
        val translation = ObjectAnimator.ofFloat(binding.welcomePic, "translationX", -0.05f, 0.05f).apply {
            duration = 50
            repeatCount = 20
            repeatMode = ValueAnimator.REVERSE
        }
        set.playTogether(rotation, translation)*/

        binding.welcomePic.alpha = 0f
        binding.welcomePic.animate().setDuration(2500).alpha(1f).withEndAction {
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                finish()
        }


    }
}