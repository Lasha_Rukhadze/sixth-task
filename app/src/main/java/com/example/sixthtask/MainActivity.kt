package com.example.sixthtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sixthtask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var newPerson : Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.changeinfo.setOnClickListener {
            nextPage()
        }
    }

    private fun nextPage(){

        val intent = Intent(this, SecondActivity::class.java)
        startActivityForResult(intent, 1)
    }

    private fun setter(person : Person){
        binding.firstnamevalue.text = person.personFirstName
        binding.lastnamevalue.text = person.personLastName
        binding.emailvalue.text = person.personEmail
        binding.birthyearvalue.text = person.personBirthYear.toString()
        binding.gendervalue.text = person.personGender
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1) {
            if (resultCode == RESULT_OK) {
                newPerson = data?.getSerializableExtra("NewPerson") as Person
                setter(newPerson)
            }
        }
    }
}